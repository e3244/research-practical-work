matplotlib==3.5.0
numpy==1.21.2
scipy==1.7.3
ipympl==0.7.0
sounddevice==0.4.5